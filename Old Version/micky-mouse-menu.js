var newJsonForMenu;
var finalJsonForMenu;

MickyMouseMenuHtml =



    '<!-- menu start -->' +
    '<div class="container-fluid mainMenuContainer" style=" display: none; min-height: 50%; min-width: 90%;">' +
    '<!-- search Box -->' +
    '<div class="row searchBar" style=" margin: auto;">' +
    '<div class="searchBar col-xs-10" style=" padding: 3%; margin-left: 8.3%;">' +
    '<!-- <form> -->' +
    '<div class="" style="width:95%;">' +
    '<input type="text" name "menuSearch" class="form-control menuSearch" autocomplete="off" placeholder ="Find a service by name (for example, Web Results)." id="menuSearch" autofocus />' +
    '</div>' +
    '<ul class="mickySearchResults list-group searchResults pre-scrollable mickyMouseUl"  style=" position: absolute; z-index: 999;" id="searchResults"></ul>' +
    '<!-- </form> -->' +
    '</div>' +
    '</div>' +


    '<div class="row pre-scrollable fatherContainer" id="innerContentBox">' +

    '<!-- MainTitles -->' +
    '<div class="row mainTitles">' +
    '<div class="col-sm-2 badBoy" style="padding-left: 2%;">' +
    '<h4>' +
    '<i class="fa fa-desktop"></i>&nbsp;&nbsp;&nbsp;&nbsp;Applications</h4>' +
    '</div>' +
    '<div class="col-sm-2 badBoy">' +
    '<h4>' +
    '<i class="fa fa-globe"></i>&nbsp;&nbsp;&nbsp;&nbsp;Services</h4>' +
    '</div>' +
    '<div class="col-sm-2 badBoy" style="text-align: center;">' +
    '<h4>' +
    '<i class="fa fa-info-circle"></i>&nbsp;&nbsp;&nbsp;&nbsp;Information for Students</h4>' +
    '</div>' +
    '<div class="col-sm-2 badBoy" style="text-align: center;">' +
    '<h4>' +
    '<i class="fa fa-info-circle"></i>&nbsp;&nbsp;&nbsp;&nbsp;Information for Staff</h4>' +
    '</div>' +
    '<div class="col-sm-2 badBoy">' +
    '<h4>' +
    '<i class="fa fa-suitcase"></i>&nbsp;&nbsp;&nbsp;&nbsp;Quick Links</h4>' +
    '</div>' +
    '</div>' +

    '<!-- content box -->' +
    '<div class=" col-sm-2 col-sm-2-5 col-xs-12 menuContentBox" id="menuContentBox1" style="border:none; text-align: left;">' +
    '<!-- Menu Will be generated automatically -->' +
    '</div>' +
    '<div class=" col-sm-2 col-sm-2-5 col-xs-12 menuContentBox" id="menuContentBox2" style="border:none; text-align: left;">' +
    '<!-- Menu Will be generated automatically -->' +
    '</div>' +
    '<div class=" col-sm-2 col-sm-2-5 col-xs-12 menuContentBox" id="menuContentBox3" style="border:none; text-align: left;"> ' +
    '<!-- Menu Will be generated automatically --> ' +
    '</div>' +
    '<div class=" col-sm-2 col-sm-2-5 col-xs-12 menuContentBox" id="menuContentBox4" style="border:none; text-align: left;"> ' +
    '<!-- Menu Will be generated automatically -->' +
    '</div>' +
    '<div class=" col-sm-2 col-sm-2-5 col-xs-12 menuContentBox" id="menuContentBox5" style="border:none; text-align: left;"> ' +
    '<!-- Menu Will be generated automatically --> ' +
    '</div>' +
    '</div>' +

    '<!-- close button -->' +
    '<div class="row" style="background-color:rgba(221,217,217,0.5); margin-top: 0.5%; padding-bottom: 0.5%;">' +
    '<div class="col-xs-12" style="text-align: center; height: 33px;">' +
    '<a href="#" id="show2">' +
    '<span class="glyphicon glyphicon-remove" aria-hidden="true" style="padding-top:1%;"></span> Close' +
    '</a>' +
    '</div>' +
    '</div>' +
    '</div>';


var mickyMouseSearchBarOnly = `<!-- SearchBar for smaller devices -->
    <div class="resSearchBar">
        <!-- search Box -->
        <div class="row searchBar" style=" margin: auto;">
        <div class="searchBar noneonmob col-xs-10" style=" padding: 3%; margin-left: 8.3%;">
        <!-- <form> -->
        <div class="" style="width:95%;">
        <input type="text" name "menuSearch" class="form-control menuSearch" autocomplete="off" placeholder ="Find a service by name (for example, Web Results)." id="menuSearch" autofocus />
        </div>
        <ul class="mickySearchResults list-group searchResults pre-scrollable mickyMouseUl"  style=" position: absolute; z-index: 999;" id="searchResults"></ul>
        <!-- </form> -->
        </div>
        </div>
    </div>`;

document.getElementById('MickyMouseMenuHtml').innerHTML = MickyMouseMenuHtml;
document.getElementById("MickyMouseMenuSearchBarOnly").innerHTML = mickyMouseSearchBarOnly;

//requesting the menu json file to array
var menuJson = new XMLHttpRequest();
menuJson.open('GET', "https://tu9gen2szj.execute-api.ap-southeast-1.amazonaws.com/prod/menu", true);
menuJson.send();
menuJson.onreadystatechange = processRequest;

//requesting the JSON file from AWS and displaying the menu
function processRequest(e) {
    if (menuJson.readyState == 4 && menuJson.status == 200) {
        var response = JSON.parse(menuJson.responseText);
        newJsonForMenu = '[' + arrayForSearchMenu(response, true) + '{"file": "end"}' + ']';
        finalJsonForMenu = JSON.parse(newJsonForMenu);
        var menuElement1 = document.getElementById('menuContentBox1');
        var menuElement2 = document.getElementById('menuContentBox2');
        var menuElement3 = document.getElementById('menuContentBox3');
        var menuElement4 = document.getElementById('menuContentBox4');
        var menuElement5 = document.getElementById('menuContentBox5');

        // Set the inner HTML of the DOM element and displaying main menu
        menuElement1.innerHTML = getHtmlListOneAndTwo(response[0]['subtree'], true);
        menuElement2.innerHTML = getHtmlListOneAndTwo(response[1]['subtree'], true);
        menuElement3.innerHTML = getHtmlListOneAndTwo(response[2]['subtree'], true);
        menuElement4.innerHTML = getHtmlListOneAndTwo(response[3]['subtree'], true);
        menuElement5.innerHTML = getHtmlListOneAndTwo(response[4]['subtree'], true);
    }
}


//returning HTML menu string to be displayed
function getHtmlListOneAndTwo(list, isTopLevel) {
    var path = '';
    for (var i = 0; i < list.length; ++i) {
        if (isTopLevel) {
            path +=
                '<div class="content">' +
                '<ul class="list-group SubTitles">' +
                '<li class="list-group-item mainTitleCss" style="font-weight:400; border:0px;">' +
                '<a class="level1" href="' + list[i].link.url + '">' + list[i].link.title + '</a>'; // Make outer ul and li
        } else {
            path +=
                '<li class="list-group-item level2Li" style="font-weight: normal; border:0px;">' +
                '<a href="' + list[i].link.url + '">' + list[i].link.title + '</a>'; // Make li in any case
        }
        if (list[i].subtree && list[i].subtree.length > 0) {
            path += '<ul  style="padding-left: 0px;">'; // start making sub-lists
            path += getHtmlListOneAndTwo(list[i].subtree, false);
            path += '</ul>'; // end the sub-lists
        }
        if (isTopLevel)
            path += '</li></ul></div>'; // close the running li and ul
        else
            path += '</li>'; // Otherwise, just close the running li
    }
    return path;
}

//creating an of the json file to be used for the search function
function arrayForSearchMenu(list, isTopLevel) {
    var path = '';
    for (var i = 0; i < list.length; ++i) {
        if (isTopLevel) {
            path +=
                ''
        } else {
            path +=
                '{ "title":' + '"' + list[i].link.title.trim() + '",' + '"description":' + '"' + list[i].link.description +
                '",' +
                '"url":' + '"' + list[i].link.url + '"' + '},';
        }
        if (list[i].subtree && list[i].subtree.length > 0) {
            path += arrayForSearchMenu(list[i].subtree, false);
        }
        if (isTopLevel)
            path += ''; // close the running li and ul
        else
            path += ''; // Otherwise, just close the running li
    }
    return path;
}

//function for search and selecting
$(document).ready(function () {

    //functoin to show menu on main button click
    $("#show").click(function () {
        $(".mainMenuContainer").slideToggle(100);
        var searchBar = document.getElementById("menuSearch");
        searchBar.value = null;
        HidingShowingUlForSearch();
    });

    //function for close button after displaying menu
    $("#show2").click(function () {
        $(".mainMenuContainer").slideToggle(100);
        var searchBar = document.getElementById("menuSearch");
        searchBar.value = null;
        HidingShowingUlForSearch();
    });

    //menu search and selecting function
    $(document.body).on("keyup", "", ".menuSearch", function (e) {
        switch (e.keyCode) {
            case 38: // if the UP key is pressed
                var ulSelected = document.getElementById('searchResults');
                var selected = document.getElementById('selected');
                if (selected.previousSibling == null) {
                    break;
                }
                var newSelected = selected.previousSibling;
                selected.removeAttribute('id');
                newSelected.setAttribute('id', 'selected');
                var searchBar = document.getElementById('menuSearch');
                searchBar.value = selected.firstChild.innerText;
                MickyAutoScroll(selected, ulSelected)
                break;
            case 40: // if the DOWN key is pressed
                var ulSelected = document.getElementById('searchResults');
                var selected = document.getElementById('selected');
                if (selected.nextSibling == null) {
                    break;
                }
                var newSelected = selected.nextSibling;
                selected.removeAttribute('id');
                newSelected.setAttribute('id', 'selected');
                var searchBar = document.getElementById('menuSearch');
                searchBar.value = selected.firstChild.innerText;
                MickyAutoScroll(selected, ulSelected)
                break;
            case 13:
                var link = document.getElementById("selected");
                link.firstChild.click();
                break;
            case 27:
                var searchBar = document.getElementById("menuSearch");
                searchBar.value = null;
                break;
        }
        if (e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40) {
            var MickyIndex = 0;
            $(".searchResults").html("");
            var searchField = $(".menuSearch").val();
            var expression = new RegExp(searchField, "i");
            HidingShowingUlForSearch();
            $.each(finalJsonForMenu, function (key, value) {
                if (value.title.search(expression) != -1 || value.description.search(expression) != -1) {
                   $(".mickySearchResults").append(`<li class="list-group-item mickyMouseResultLi" indexOf= "${MickyIndex}" style= "border:none; min-width: 95%;">\
                    <a target="_blank" href="${value.url}">\
                    <p>${value.title}</p>\
                    <p id="resultsDescription" style="color: #757373">${value.description}</p></a></li>`);
                    console.log(document.getElementById("selected"));
                    MickyIndex++;
                    scrollList();
                }
            });
        }
    });

    function MickyAutoScroll(li, ul) {
        let newSelected = li.getAttribute("indexOf") - 1; // index
        var elHeight = $(li).height(); // got li height
        var scrollTop = $(ul).scrollTop(); //built in function
        var viewport = scrollTop + $(ul).height(); // get main ul height
        var elOffset = elHeight * newSelected; // multiply n indexOf
        if (elOffset < scrollTop || (elOffset + elHeight) > viewport)
            $(ul).scrollTop(elOffset + 60);
    }


});

//Hiding and showing the search results
function HidingShowingUlForSearch() {
    if (document.getElementById('menuSearch').value == "") {
        document.getElementById('searchResults').style.display = "none";

    } else {
        document.getElementById('searchResults').style.border = "1px solid #cecece";
        document.getElementById('searchResults').style.display = "block";

    }
}

// selecting the search results first element
function scrollList() {
    var list = document.getElementById('searchResults'); // targets the <ul>
    var first = list.firstChild; // targets the first <li>
    first.setAttribute('id', 'selected');
}